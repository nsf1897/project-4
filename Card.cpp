#include "Card.h"

using namespace std;

  //EFFECTS Initializes Card to the Two of Spades
  Card::Card()
  	: rank(RANK_TWO), suit(SUIT_SPADES) {}

  //REQUIRES rank is one of "Two", "Three", "Four", "Five", "Six", "Seven",
  //  "Eight", "Nine", "Ten", "Jack", "Queen", "King", "Ace"
  //  suit is one of "Spades", "Hearts", "Clubs", "Diamonds"
  //EFFECTS Initializes Card to specified rank and suit
  Card::Card(const std::string &rank_in, const std::string &suit_in)
	: rank(rank_in), suit(suit_in) {}

  //EFFECTS Returns the rank
  std::string Card::get_rank() const{
  	return Card::rank;
  }

  //EFFECTS Returns the suit.  Does not consider trump.
  std::string Card::get_suit() const{
  	return Card::suit;
  }

  //EFFECTS Returns the suit
  //HINT: the left bower is the trump suit!
  std::string Card::get_suit(const std::string &trump) const{
  	if ((Card::rank == Card::RANK_JACK) && (Card::suit == Suit_next(trump))){
  		return trump;
  	}
  	
  	else{
  		return suit;
  	}	 
  }

  //EFFECTS Returns true if card is a face card (Jack, Queen, King or Ace)
  bool Card::is_face() const{
  	if ((Card::rank == Card::RANK_JACK) || (Card::rank == Card::RANK_QUEEN) || 
  		(Card::rank == Card::RANK_KING) || (Card::rank == Card::RANK_ACE)){
  		return true;
  	}
  	else{
  		return false;
  	}
  }

  //EFFECTS Returns true if card is the Jack of the trump suit
  bool Card::is_right_bower(const std::string &trump) const{
  	if ((rank == RANK_JACK) && (suit == trump)){
  		return true;
  	}
  	else{
  		return false;
  	}
  }

  //EFFECTS Returns true if card is the Jack of the next suit
  bool Card::is_left_bower(const std::string &trump) const{
  	if ((Card::rank == Card::RANK_JACK) && (Card::suit == Suit_next(trump))){
  		return true;
  	}
  	else{
  		return false;
  	}
  }

  //EFFECTS Returns true if the card is a trump card.  All cards of the trump
  // suit are trump cards.  The left bower is also a trump card.
  bool Card::is_trump(const std::string &trump) const{
  	if ((Card::suit == trump) || ((Card::rank == Card::RANK_JACK) && (Card::suit == Suit_next(trump)))){
  		return true;
  	}
  	else{
  		return false;
  	}
  }

//EFFECTS Returns true if lhs is lower value than rhs.
//  Does not consider trump.
bool operator<(const Card &lhs, const Card &rhs){
	int rhs_rank = 0;
	int lhs_rank = 0;
	for(int i = 0; i < 13; i++){
		if (RANK_NAMES_BY_WEIGHT[i] == lhs.get_rank()){
			lhs_rank = i;
		}
		if (RANK_NAMES_BY_WEIGHT[i] == rhs.get_rank()){
			rhs_rank = i;
		}
	}
	if (lhs_rank < rhs_rank){
		return true;
	}
	else{
		return false;
	}
}

//EFFECTS Returns true if lhs is higher value than rhs.
//  Does not consider trump.
bool operator>(const Card &lhs, const Card &rhs){
	int rhs_rank= 0;
	int lhs_rank = 0;
	for(int i = 0; i < 13; i++){
		if (RANK_NAMES_BY_WEIGHT[i] == lhs.get_rank()){
			lhs_rank = i;
		}
		if (RANK_NAMES_BY_WEIGHT[i] == rhs.get_rank()){
			rhs_rank = i;
		}
	}
	if (lhs_rank > rhs_rank){
		return true;
	}
	else{
		return false;
	}
}

//EFFECTS Returns true if lhs is same card as rhs.
//  Does not consider trump.
bool operator==(const Card &lhs, const Card &rhs){
	if((lhs.get_suit() == rhs.get_suit()) && (lhs.get_rank() == rhs.get_rank())){
		return true;
	}
	else{
		return false;
	}
}

//EFFECTS Returns true if lhs is not the same card as rhs.
//  Does not consider trump.
bool operator!=(const Card &lhs, const Card &rhs){
	if((lhs.get_suit() == rhs.get_suit()) && (lhs.get_rank() == rhs.get_rank())){
		return false;
	}
	else{
		return true;
	}
}

//EFFECTS returns the next suit, which is the suit of the same color
std::string Suit_next(const std::string &suit){
	if (suit == Card::SUIT_SPADES){
		return Card::SUIT_CLUBS;
	}
	else if (suit == Card::SUIT_CLUBS){
		return Card::SUIT_SPADES;
	}
	else if (suit == Card::SUIT_HEARTS){
		return Card::SUIT_DIAMONDS;
	}
	else {
		return Card::SUIT_HEARTS;
	}
}

//EFFECTS Prints Card to stream, for example "Two of Spades"
std::ostream & operator<<(std::ostream &os, const Card &card){
	os << card.get_rank() << " of " << card.get_suit();
	return os;
}

//EFFECTS Returns true if a is lower value than b.  Uses trump to determine
// order, as described in the spec.
bool Card_less(const Card &a, const Card &b, const std::string &trump){
	if ((a.is_trump(trump)) && (b.is_trump(trump))){
		if (a.get_rank() < b.get_rank()){
			return true;
		}
		else{
			return false;
		}
	}
	else if (a.is_trump(trump)){
		return false;
	}
	else if (b.is_trump(trump)){
		return true;
	}
	else{
		if (a.get_rank() < b.get_rank()){
			return true;
		}
		else{
			return false;
		}
	}
}

//EFFECTS Returns true if a is lower value than b.  Uses both the trump suit
//  and the suit led to determine order, as described in the spec.
bool Card_less(const Card &a, const Card &b, const Card &led_card, const std::string &trump){
	if (trump == led_card.get_suit()){
		if ((a.is_trump(trump)) && (b.is_trump(trump))){
			if (a.get_rank() < b.get_rank()){
				return true;
			}
			else{
				return false;
			}
		}
		else if (a.is_trump(trump)){
			return false;
		}
		else if (b.is_trump(trump)){
			return true;
		}
		else{
			if (a.get_rank() < b.get_rank()){
				return true;
			}
			else{
				return false;
			}
		}
	}



	else {
		if ((a.is_trump(trump)) && (b.is_trump(trump))){
			if (a.get_rank() < b.get_rank()){
				return true;
			}
			else{
				return false;
			}
		}
		else if (a.is_trump(trump)){
			return false;
		}
		else if (b.is_trump(trump)){
			return true;
		}
		else{
			if ((a.get_suit() == led_card.get_suit()) && (b.get_suit() == led_card.get_suit())){
				if (a.get_rank() < b.get_rank()){
					return true;
				}
				else{
					return false;
				}
			}
			else if (a.get_suit() == led_card.get_suit()){
				return false;
			}
			else if (b.get_suit() == led_card.get_suit()){
				return true;
			}
			else{
				if (a.get_rank() < b.get_rank()){
					return true;
				}
				else{
					return false;
				}
			}
		}
	}

}



